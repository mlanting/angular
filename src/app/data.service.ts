import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get('https://jsonplaceholder.typicode.com/users')
  }

  getUser(userId) {
    return this.http.get('https://jsonplaceholder.typicode.com/users/' + userId)
  }

  getPosts() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts')
  }

  getPostsByUser(userId) {
    return this.http.get('https://jsonplaceholder.typicode.com/posts/' + userId)
  }

  getPost(postId) {
    return this.http.get('https://jsonplaceholder.typicode.com/posts/' + postId)
  }

  getCommentsByPost(postId) {
    return this.http.get('https://jsonplaceholder.typicode.com/comments?postId=' + postId)    
  }

  getCommentsByMail(email) {
    return this.http.get('https://jsonplaceholder.typicode.com/comments?email=' + email)    
  }

  getAlbumsByUser(userId) {
    return this.http.get('https://jsonplaceholder.typicode.com/albums?userId=' + userId)
  }

  getAlbum(albumId) {
    return this.http.get('https://jsonplaceholder.typicode.com/albums/' + albumId)
  }

  getPhotosByAlbum(albumId) {
    return this.http.get('https://jsonplaceholder.typicode.com/photos?albumId=' + albumId)
  }

  getTodosByUser(userId) {
    return this.http.get('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
  }
}
