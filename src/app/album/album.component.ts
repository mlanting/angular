import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {

  photos$: Object;
  album$: Object;

  constructor(private data: DataService, private route: ActivatedRoute) {
    this.route.params.subscribe( params => this.album$ = params.id )
  }

  ngOnInit() {
    this.data.getPhotosByAlbum(this.album$).subscribe(
      data => this.photos$ = data
    )

    this.data.getAlbum(this.album$).subscribe(
      data => this.album$ = data
    )

  }

}
