import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, ActivationEnd } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  post$: Object;
  comments$: Object;

  constructor(private data: DataService, private route: ActivatedRoute) {
    this.route.params.subscribe( params => this.post$ = params.id)
  }

  ngOnInit() {
    this.data.getPost(this.post$).subscribe(
      data => this.post$ = data
    )

    this.data.getCommentsByPost(this.post$).subscribe(
      data => this.comments$ = data
    )
  }

}
