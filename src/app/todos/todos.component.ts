import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, ActivationEnd } from '@angular/router';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {

  todos$: Object;
  user$: Object;

  constructor(private data: DataService, private route: ActivatedRoute) {
    this.route.params.subscribe( params => this.user$ = params.id)
  }

  ngOnInit() {
    this.data.getTodosByUser(this.user$).subscribe(
      data => this.todos$ = data
    )
    
    this.data.getUser(this.user$).subscribe(
      data => this.user$ = data
    )
  }

}
