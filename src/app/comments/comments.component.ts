import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})

export class CommentsComponent implements OnInit {

  comments$: Object;
  user$: Object;

  constructor(private data: DataService, private route: ActivatedRoute) { 
    this.route.params.subscribe( params => this.user$ = params.id )
  }

  ngOnInit() {
    this.data.getUser(this.user$).subscribe(
      data => this.user$ = data
    )

    /*
    * Er zijn geen comments die bij de users horen, daarom heb ik voor proof of concept het email adres van de eerste comment gepakt.
    * Om de comments van een user te krijgen moet je het email adres vervangen met this.user$['email']
    */
    this.data.getCommentsByMail('Eliseo@gardner.biz').subscribe(
      data => this.comments$ = data
    )

  }

}
